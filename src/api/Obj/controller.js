import { success, notFound } from '../../services/response/'
import { Obj } from '.'

export const create = ({ body }, res, next) => {
  let keys = body && Object.keys(body);
  let key = keys && keys[0];
  if (key === undefined)
    throw Error("Key is missing.");
  let value = body[key];
  let data = {
    key: key,
    value: value,
    timestamp: new Date()
  };
  return Obj.create(data)
    .then((obj) => { success(res, obj, 201) })
    .catch(next)
}


export const show = ({ params, query }, res, next) => {
  let timestamp = query && query.timestamp;
  let key = params && params.key;
  if (key === undefined)
    throw ("Key is missing.")
  let queryData = { key: key };
  if (timestamp) {
    timestamp = parseInt(timestamp);
    queryData.timestamp = { $lte: timestamp * 1000 };
  }
  return Obj.find(queryData)
    .sort({ timestamp: -1 })
    .limit(1)
    .then((list) => {
      let obj = list[0];
      if (obj)
        success(res, obj)
      else
        notFound(res)
    })
    .catch(next)
}
