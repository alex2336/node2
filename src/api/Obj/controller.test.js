const rewiremock = require('rewiremock').default;
import { Obj } from '.';

jest.mock('.', () => ({
  Obj: {
    create: jest.fn(),
    find: jest.fn()
  }
}))

import { create, show } from './controller'


beforeEach(async () => {
  //Mock date
  const mockedDate = new Date()
  const originalDate = Date
  global.Date = jest.fn(() => mockedDate)
  global.Date.setDate = originalDate.setDate
})

describe('Create', () => {
  it('Key is missing', () => {
    expect(() => { create({ body: {} }) }).toThrow('Key is missing')
  })

  it('Should call Obj.create', () => {
    try {
      create({ body: { key: "value" } })
    } catch (e) {

    }
    expect(Obj.create).toHaveBeenCalled()
    expect(Obj.create).toHaveBeenCalledWith({ "key": "key", "timestamp": new Date(), "value": "value" })
  })
})

describe('Show', () => {
  it('Key is missing', () => {
    expect(() => { show({}) }).toThrow('Key is missing')
  })
  it('No timestamp query', () => {
    try {
      show({ params: { key: "key" } })
    } catch (e) {
    }
    expect(Obj.find).toHaveBeenCalled()
    expect(Obj.find).toHaveBeenCalledWith({ "key": "key" })
  })
  it('With timestamp query', () => {
    Obj.find.mockReset();
    try {
      show({ params: { key: "key" }, query: { timestamp: "123" } })
    } catch (e) {
    }
    expect(Obj.find).toHaveBeenCalled()
    expect(Obj.find).toHaveBeenCalledWith({ "key": "key", timestamp: { "$lte": 123000 } })
  })
})

