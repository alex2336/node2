import { Router } from 'express'
import { create, show } from './controller'
import { schema } from './model'
export Obj, { schema } from './model'

const router = new Router()
const { value } = schema.tree

/**
 * @api {post} /object Create obj
 * @apiName CreateObj
 * @apiGroup Obj
 * @apiParam value Obj's value.
 * @apiSuccess {Object} obj Obj's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Obj not found.
 */
router.post('/',
  create)

/**
 * @api {get} /object/:id Retrieve obj
 * @apiName RetrieveObj
 * @apiGroup Obj
 * @apiSuccess {Object} obj Obj's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Obj not found.
 */
router.get('/:key',
  show)

export default router
