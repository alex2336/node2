import mongoose, { Schema } from 'mongoose'

const objSchema = new Schema({
  key: {
    type: String
  },
  value: {
    type: String
  },
  timestamp: {
    type: Number
  }
}, {
    toJSON: {
      virtuals: true,
      transform: (obj, ret) => { delete ret._id }
    }
  })

objSchema.index({ key: 1, timestamp: 1 });

const model = mongoose.model('Obj', objSchema)

export const schema = model.schema
export default model
